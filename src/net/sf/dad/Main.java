/*
 * $Id: Main.java,v 1.3 2003/08/04 03:40:15 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/Main.java,v $
 */
 
package net.sf.dad;

import java.util.prefs.Preferences;

import javax.swing.UIManager;

import net.sf.dad.gui.MainWindow;
import net.sf.dad.gui.Messages;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.3 $
 */
public class Main {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}
		
		Messages.setLanguage(
			Preferences.userNodeForPackage(MainWindow.class).get("Language", "en")); //$NON-NLS-1$

		new MainWindow().show();
	}
}
