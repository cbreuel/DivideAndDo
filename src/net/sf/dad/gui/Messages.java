/*
 * $Id: Messages.java,v 1.1.1.1 2003/07/25 22:46:23 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/gui/Messages.java,v $
 */

package net.sf.dad.gui;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.1.1.1 $
 */
public class Messages {

	private static String bundleName = "net.sf.dad.gui.en"; //$NON-NLS-1$

	private static ResourceBundle resourceBundle =
		ResourceBundle.getBundle(bundleName);

	/**
	 * 
	 */
	private Messages() {
	}

	/**
	 * @param key
	 * @return
	 */
	public static String getString(String key) {
		try {
			return resourceBundle.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
	
	public static void setLanguage(String language) {
		bundleName = "net.sf.dad.gui." + language;
		resourceBundle = ResourceBundle.getBundle(bundleName);
	}
}
