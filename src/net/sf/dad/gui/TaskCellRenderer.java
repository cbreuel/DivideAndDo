/*
 * $Id: TaskCellRenderer.java,v 1.2 2003/07/29 22:20:57 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/gui/TaskCellRenderer.java,v $
 */
 
package net.sf.dad.gui;

import net.sf.dad.model.Item;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.2 $
 */
public class TaskCellRenderer extends DefaultTreeCellRenderer {
	
	public static ImageIcon taskWithSubtasksIcon;
	public static ImageIcon taskWithoutSubtasksIcon;
	public static ImageIcon taskCancelledIcon;
	public static ImageIcon taskDoneIcon;
	
	static {
		taskWithSubtasksIcon = new ImageIcon(TaskCellRenderer.class.getResource("img/task_with_subtasks.gif"));
		taskWithoutSubtasksIcon = new ImageIcon(TaskCellRenderer.class.getResource("img/task_without_subtasks.gif"));
		taskCancelledIcon = new ImageIcon(TaskCellRenderer.class.getResource("img/task_cancelled.gif"));
		taskDoneIcon = new ImageIcon(TaskCellRenderer.class.getResource("img/task_done.gif"));
	}

	public Component getTreeCellRendererComponent(
			JTree tree,
			Object value,
			boolean sel,
			boolean expanded,
			boolean leaf,
			int row,
			boolean hasFocus) {

		super.getTreeCellRendererComponent(
			tree, value, sel,
			expanded, leaf, row,
			hasFocus);
			
		Item item = ((TaskTreeNode)value).getItem(); 
		
		if (!item.getIsCategory()) {
			if (item.getDone()) {
				setIcon(taskDoneIcon);
			} else if (item.getCancelled()) {
				setIcon(taskCancelledIcon);
			} else {
				if (item.hasChildren()) {
					setIcon(taskWithSubtasksIcon);
				} else {
					setIcon(taskWithoutSubtasksIcon);
				}
			}
		}
		
		return this;
   }

}
