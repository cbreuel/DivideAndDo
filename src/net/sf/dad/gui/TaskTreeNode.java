/*
 * $Id: TaskTreeNode.java,v 1.3 2003/09/30 15:31:13 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/gui/TaskTreeNode.java,v $
 */
 
package net.sf.dad.gui;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;

import net.sf.dad.model.Item;

/**
 * This class extends DefaultMutableTreeNode to implement specific
 * functionality for the Application.
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.3 $
 */
public class TaskTreeNode extends DefaultMutableTreeNode {

	/** The <code>Item</code> object associated whith this node. */
	private Item item;
	
	/** The TreeModel for this node */
	private DefaultTreeModel model = null;
	
	/** The tree in which the node is contained */
	private TaskTree tree;

	/**
	 * Constructs a new TaskTreeNode.
	 * 
	 * @param obj the userObject
	 * @param isCategory wether this node's Item should be a category
	 * @param model the TreeModel for the container tree
	 * @param tree the container tree for this node
	 */
	public TaskTreeNode(Object obj, boolean isCategory, DefaultTreeModel model, TaskTree tree){
		super(obj, true);

		this.model = model;
		this.tree = tree;
		this.item = new Item();
		item.setIsCategory(isCategory);
		item.setDescription(obj.toString());
	}

	/**
	 * Gets the current <code>Item</code> associated with
	 * this node.
	 * 
	 * @return the Item property of this node.
	 */
	public Item getItem() {
		return item;
	}

	/**
	 * Sets the current <code>Item</code> associated with
	 * this node.
	 * 
	 * @param item the <code>Item</code> to associate with this node
	 */
	public void setItem(Item item) {
		this.item = item;
	}

	/**
	 * Sets the Done property for this node and its associated
	 * <code>Item</code>. Also refreshes the view of the
	 * container Tree to reflect the change.
	 * 
	 * @param done indicates whether the Item is done.
	 */
	public void setDone(boolean done) {
		this.item.setDone(done);
		if (done && !this.tree.isViewDone()) {
			this.model.removeNodeFromParent(this);
			this.tree.reload();
		}
	}
	
	/**
	 * Sets the Cancelled property for this node and its associated
	 * <code>Item</code>. Also refreshes the view of the
	 * container Tree to reflect the change.
	 * 
	 * @param cancelled indicates whether the Item is cancelled.
	 */
	public void setCancelled(boolean cancelled) {
		this.item.setCancelled(cancelled);
		if (cancelled && !this.tree.isViewCancelled()) {
			this.model.removeNodeFromParent(this);
			this.tree.reload();
		}
	}
	
	/**
	 * Sets the node's Item's Expanded property. 
	 * 
	 * @param expanded new new value for the property.
	 */
	public void setExpanded(boolean expanded) {
		this.item.setExpanded(expanded);
	}
	
	/**
	 * Returns whether the node's Item's Expanded property is
	 * set.
	 * 
	 * @return the value of the node's Item's Expanded property.
	 */
	public boolean getExpanded() {
		return this.item.getExpanded();
	}

	/**
	 * A recursive method that rebuilds the tree. All the
	 * node's children are displayed, and the method is then
	 * called on them.  
	 */
	public void rebuildTree() {
		this.removeAllChildren();
		Item[] childrenItems = this.item.getChildren();
		for (int i=0; i<childrenItems.length; ++i) {
			Item nextItem = childrenItems[i];

			if ((!nextItem.getDone() && !nextItem.getCancelled()) ||
				(nextItem.getDone() && this.tree.isViewDone()) ||
				(nextItem.getCancelled() && this.tree.isViewCancelled())) {
					
				TaskTreeNode newChild = new TaskTreeNode(nextItem.getDescription(), nextItem.getIsCategory(), this.model, this.tree);
				newChild.setItem(nextItem);
				this.model.insertNodeInto(newChild, this, this.getChildCount());
				newChild.rebuildTree();

			}
		}
	}
	
	/**
	 * Removes a node from its parent. 
	 */
	public void delete() {
		this.item.removeFromParent();
		this.model.removeNodeFromParent(this);
		this.tree.reload();
	}

	/**
	 * Changes a node's position amog it's siblings.
	 * 
	 * @param offset the number of positions to move. Negative is up, positive is down.
	 */
	public void move(int offset) {
		Item itemParent = this.item.getParent();
		int newIndex = this.model.getIndexOfChild(this.getParent(), this) + offset;
		this.item.removeFromParent();
		itemParent.insertChild(newIndex, this.item);
	}
	

/* **********************************************************************
 * Superclass javax.swing.tree.MutableTreeNode
 ************************************************************************/ 

	/**
	 * Returns always false, because all nodes can have
	 * children.
	 * 
	 * @return <code>false</code> 
	 * @see javax.swing.tree.MutableTreeNode#remove(int)
	 */
	public boolean isLeaf() {
		return false;
	}

	/**
	 * Returns always false, because all nodes can have
	 * children.
	 *
	 * @return <code>true</code> 
	 * @see javax.swing.tree.TreeNode#getAllowsChildren()
	 */
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * Sets the node's user object, and set the node's item's
	 * description to the String representation of the object.
	 *
	 * @param obj the new user object. 
	 * @see javax.swing.tree.MutableTreeNode#setUserObject(java.lang.Object)
	 */
	public void setUserObject(Object obj) {
		super.setUserObject(obj);
		this.item.setDescription(obj.toString());
	}

	/**
	 * Inserts a child node in a specific position. Also sets
	 * the node's Item as child of this node's Item, at the
	 * same position.
	 * 
	 * @see javax.swing.tree.MutableTreeNode#insert(javax.swing.tree.MutableTreeNode, int)
	 */
	public void insert(MutableTreeNode arg0, int arg1) {
		item.insertChild(arg1, ((TaskTreeNode)arg0).getItem());
		super.insert(arg0, arg1);
	}

	/**
	 * Returns the description of node's Item.
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.item.toString();
	}

}
