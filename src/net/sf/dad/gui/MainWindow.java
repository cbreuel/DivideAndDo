/*
 * $Id: MainWindow.java,v 1.11 2004/07/22 02:06:32 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/gui/MainWindow.java,v $
 */
 
package net.sf.dad.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.prefs.Preferences;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SpinnerDateModel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileFilter;

import net.sf.dad.FileHandler;
import net.sf.dad.model.Item;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.11 $
 */
public class MainWindow extends JFrame {

	private static final int WINDOW_WIDTH = 600;
	private static final int WINDOW_HEIGHT = 600;
	
	private static final String VERSION = "0.6";
	private static final String WINDOW_TITLE = "Divide And Do";
	
	private MainWindow thisWindow = this;
	
	private TaskTree tree;
	private FileHandler fileHandler = new FileHandler();
	
	private EditPanel editPanel;
	private JSplitPane splitPane;
	private JCheckBoxMenuItem menuViewDone;
	private JCheckBoxMenuItem menuViewCancelled;
	
	static JFileChooser fc = new JFileChooser();
	
	private ItemMenuHandler itemMenuHandler;

	private String language;
	
	private JMenuItem menuOptionsLanguageEn;
	private JMenuItem menuOptionsLanguagePtBR;

	/**
	 * @throws java.awt.HeadlessException
	 */
	public MainWindow() throws HeadlessException {

		super(WINDOW_TITLE); //$NON-NLS-1$
		
		fc.setCurrentDirectory(fileHandler.getCurrentFile());
		fc.setFileFilter(new FileFilter() {
				public boolean accept(File f) {
					return 
						(f.getName().toLowerCase().endsWith(FileHandler.DEFAULT_EXTENSION)) ||
						(f.getName().toLowerCase().endsWith(FileHandler.COMPRESSED_EXTENSION)) ||
						(f.isDirectory());
				}
				public String getDescription() {
					return "DAD files (*." + FileHandler.DEFAULT_EXTENSION + ", *." + FileHandler.COMPRESSED_EXTENSION + ")";
				}
			});
		
		this.setIconImage(Toolkit.getDefaultToolkit().createImage(this.getClass().getResource("img/task_with_subtasks.gif"))); //$NON-NLS-1$
		this.setBounds(
			(Toolkit.getDefaultToolkit().getScreenSize().width - WINDOW_WIDTH) / 2,
			(Toolkit.getDefaultToolkit().getScreenSize().height - WINDOW_HEIGHT) / 2,
			WINDOW_WIDTH,
			WINDOW_HEIGHT);
		this.getContentPane().setLayout(new BorderLayout());
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowListener() {
			public void windowClosing(WindowEvent e) {
				thisWindow.editPanel.save();
				if (askToSaveFile()) {
					savePreferences();
					System.exit(0);
				}
			}
			public void windowActivated(WindowEvent e) {
			}
			public void windowClosed(WindowEvent e) {
			}
			public void windowDeactivated(WindowEvent e) {
			}
			public void windowDeiconified(WindowEvent e) {
			}
			public void windowIconified(WindowEvent e) {
			}
			public void windowOpened(WindowEvent e) {
			}
		});
		
		// SplitPane
		splitPane = new JSplitPane();
		this.getContentPane().add(splitPane, BorderLayout.CENTER);
		splitPane.setResizeWeight(1);

		// Tree
		tree = new TaskTree();
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				TaskTreeNode node = (TaskTreeNode)tree.getLastSelectedPathComponent();
				if (node == null || node == thisWindow.tree.getModel().getRoot()) {
					editPanel.showItem(null);
				} else {
					editPanel.showItem(node.getItem());
				}
			}
		});
		JScrollPane treeScrollPane = new JScrollPane(tree);
		splitPane.add(treeScrollPane, JSplitPane.LEFT);
		splitPane.add(treeScrollPane, JSplitPane.LEFT);
		
		this.itemMenuHandler = ItemMenuHandler.getInstance();

		// Edit Panel
		editPanel = new EditPanel();
		splitPane.add(editPanel, JSplitPane.RIGHT);
		
		// Menu
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menuFile = new JMenu(Messages.getString("MainWindow.File_3")); //$NON-NLS-1$
		menuFile.setMnemonic(KeyEvent.VK_F);
		JMenuItem menuFileNew = new JMenuItem(Messages.getString("MainWindow.New_4"), KeyEvent.VK_N); //$NON-NLS-1$
		menuFileNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				newFile();
			}
		});
		menuFileNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		menuFile.add(menuFileNew);
		JMenuItem menuFileOpen = new JMenuItem(Messages.getString("MainWindow.Open..._5"), KeyEvent.VK_O); //$NON-NLS-1$
		menuFileOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				openFile();
			}
		});
		menuFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		menuFile.add(menuFileOpen);
		JMenuItem menuFileSave = new JMenuItem(Messages.getString("MainWindow.Save_6"), KeyEvent.VK_S); //$NON-NLS-1$
		menuFileSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveFile();
			}
		});
		menuFileSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		menuFile.add(menuFileSave);
		JMenuItem menuFileSaveAs = new JMenuItem(Messages.getString("MainWindow.Save_as..._7"), KeyEvent.VK_A); //$NON-NLS-1$
		menuFileSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveFileAs();
			}
		});
		menuFile.add(menuFileSaveAs);
		menuFile.addSeparator();
		JMenuItem menuFileExit = new JMenuItem(Messages.getString("MainWindow.Exit_8"), KeyEvent.VK_X); //$NON-NLS-1$
		menuFileExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		menuFile.add(menuFileExit);
		menuBar.add(menuFile);

		JMenu menuView = new JMenu(Messages.getString("MainWindow.View")); //$NON-NLS-1$
		menuView.setMnemonic(KeyEvent.VK_V);
		menuViewDone = new JCheckBoxMenuItem(Messages.getString("MainWindow.Done"), false); //$NON-NLS-1$
		menuViewDone.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				thisWindow.tree.setViewDone(e.getStateChange() == ItemEvent.SELECTED);
			}
		});
		menuView.add(menuViewDone);
		menuViewCancelled = new JCheckBoxMenuItem(Messages.getString("MainWindow.Cancelled"), false); //$NON-NLS-1$
		menuViewCancelled.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				thisWindow.tree.setViewCancelled(e.getStateChange() == ItemEvent.SELECTED);
			}
		});
		menuView.add(menuViewCancelled);
		menuBar.add(menuView);

		menuBar.add(itemMenuHandler.getMenu());

		JMenu menuOptions = new JMenu(Messages.getString("MainWindow.Options")); //$NON-NLS-1$
		menuOptions.setMnemonic(KeyEvent.VK_O);
		JMenu menuOptionsLanguage = new JMenu(Messages.getString("MainWindow.Language")); //$NON-NLS-1$
		menuOptionsLanguage.setMnemonic(KeyEvent.VK_L);
		ButtonGroup languageButtonGroup = new ButtonGroup();
		menuOptionsLanguageEn = new JRadioButtonMenuItem("English");
		languageButtonGroup.add(menuOptionsLanguageEn);
		menuOptionsLanguageEn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setLanguage("en"); //$NON-NLS-1$
			}
		});
		menuOptionsLanguage.add(menuOptionsLanguageEn);
		menuOptionsLanguagePtBR = new JRadioButtonMenuItem("Portugu�s (Brasil)");
		languageButtonGroup.add(menuOptionsLanguagePtBR);
		menuOptionsLanguagePtBR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setLanguage("pt-br"); //$NON-NLS-1$
			}
		});
		menuOptionsLanguage.add(menuOptionsLanguagePtBR);
		menuOptions.add(menuOptionsLanguage);
		menuBar.add(menuOptions);

		JMenu menuHelp = new JMenu(Messages.getString("MainWindow.Help_9")); //$NON-NLS-1$
		menuHelp.setMnemonic(KeyEvent.VK_H);
		JMenuItem menuHelpAbout = new JMenuItem(Messages.getString("MainWindow.About..._10"), KeyEvent.VK_A); //$NON-NLS-1$
		menuHelpAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(
					thisWindow,
					"<html><b><font color='navy'>Divide And Do</font></b><br><br>" + //$NON-NLS-1$
					"<b>" + Messages.getString("MainWindow.Version_3") + ":</b> " + VERSION + "<br>" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					"<b>" + Messages.getString("MainWindow.Java_6") + ":</b> " + System.getProperty("java.vm.version") + "<br>" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					"<b>" + Messages.getString("MainWindow.OS_11") + ":</b> " + System.getProperty("os.name") + " " + System.getProperty("os.version") + "<br>" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
					"<b>" + Messages.getString("MainWindow.Author_18") + ":</b> Cristiano Breuel<br>" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					"<b>" + Messages.getString("MainWindow.URL_21") + ":</b> <a href='http://sourceforge.net/projects/dad/'>http://sourceforge.net/projects/dad/</a><br>" + //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					"</html>", //$NON-NLS-1$
					Messages.getString("MainWindow.About_24"), //$NON-NLS-1$
					JOptionPane.INFORMATION_MESSAGE,
					new ImageIcon(thisWindow.getIconImage()));
			}
		});
		menuHelp.add(menuHelpAbout);
		menuBar.add(menuHelp);
		setJMenuBar(menuBar);
		
		loadPreferences();
		
	}
	
	private void setLanguage(String lang) {
		JOptionPane.showMessageDialog(this, Messages.getString("MainWindow.The_language_will_change_the_next_time_the_program_is_started"), Messages.getString("MainWindow.Information"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
		this.language = lang;
	}

	private void savePreferences() {
		Preferences node = Preferences.userNodeForPackage(this.getClass());

		if (fileHandler.hasOpenFile()) {
			node.put("LastOpenFile", fileHandler.getCurrentFile().getAbsolutePath()); //$NON-NLS-1$
		} else {
			node.put("LastOpenFile", ""); //$NON-NLS-1$ //$NON-NLS-2$
		}

		int extendedState = this.getExtendedState();
		
		node.putInt("WindowExtendedState", extendedState); //$NON-NLS-1$

		if ((extendedState & JFrame.MAXIMIZED_BOTH) == 0) {
			node.putInt("WindowLeft", this.getX()); //$NON-NLS-1$
			node.putInt("WindowTop", this.getY()); //$NON-NLS-1$
			node.putInt("WindowWidth", this.getWidth()); //$NON-NLS-1$
			node.putInt("WindowHeight", this.getHeight()); //$NON-NLS-1$
		}
		
		node.putInt("SplitPanePosition", this.splitPane.getDividerLocation()); //$NON-NLS-1$
		
		node.putBoolean("ViewDone", this.menuViewDone.getState()); //$NON-NLS-1$
		node.putBoolean("ViewCancelled", this.menuViewCancelled.getState()); //$NON-NLS-1$
		
		node.put("Language", this.language); //$NON-NLS-1$
	}
	
	private void loadPreferences() {
		Preferences node = Preferences.userNodeForPackage(this.getClass());

		String fileName = node.get("LastOpenFile", null); //$NON-NLS-1$
		if (fileName != null && !fileName.equals("")) { //$NON-NLS-1$
			doOpenFile(new File(fileName), false);
		}
		
		this.setBounds(
				node.getInt("WindowLeft", this.getX()), //$NON-NLS-1$
				node.getInt("WindowTop", this.getY()), //$NON-NLS-1$
				node.getInt("WindowWidth", this.getWidth()), //$NON-NLS-1$
				node.getInt("WindowHeight", this.getHeight()) //$NON-NLS-1$
			);
		
		this.setExtendedState(node.getInt("WindowExtendedState", this.getExtendedState())); //$NON-NLS-1$

		this.splitPane.setDividerLocation(node.getInt("SplitPanePosition", this.splitPane.getDividerLocation())); //$NON-NLS-1$

		this.menuViewDone.setSelected(node.getBoolean("ViewDone", false)); //$NON-NLS-1$
		this.menuViewCancelled.setSelected(node.getBoolean("ViewCancelled", false)); //$NON-NLS-1$
		
		this.language = node.get("Language", "en"); //$NON-NLS-1$
		if (this.language.equals("en")) {
			menuOptionsLanguageEn.setSelected(true);
		}
		if (this.language.equals("pt-br")) {
			menuOptionsLanguagePtBR.setSelected(true);
		}
	}
	
	// File-handling routines
	
	private boolean askToSaveFile() {
		int returnValue = JOptionPane.showConfirmDialog(this, Messages.getString("MainWindow.Save_the_current_file__11"), Messages.getString("MainWindow._12"), JOptionPane.YES_NO_CANCEL_OPTION); //$NON-NLS-1$ //$NON-NLS-2$
		if (returnValue == JOptionPane.CANCEL_OPTION) {
			return false;
		} else if (returnValue == JOptionPane.YES_OPTION) {
			saveFile();
			return true;
		} else { // returnValue == JOptionPane.NO_OPTION
			return true;
		}
	}
	
	private void newFile() {
		if (askToSaveFile()) {
			fileHandler.newFile();
			this.tree.reset();
			this.setTitle(WINDOW_TITLE);
		}
	}
	
	private void openFile() {
		if (askToSaveFile()) {
			if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				doOpenFile(fc.getSelectedFile(), true);
			}
		}
	}
	
	private void doOpenFile(File f, boolean generateErrorIfNotFound) {
		Item root = null;
		try {
			root = fileHandler.open(f);
			this.tree.setRootItem(root);
			this.setTitle(WINDOW_TITLE + " - " + f.getAbsolutePath());
			fc.setCurrentDirectory(fileHandler.getCurrentFile());
		} catch (FileNotFoundException e) {
			if (generateErrorIfNotFound) {
				JOptionPane.showMessageDialog(this, e.getMessage(), Messages.getString("MainWindow.Error"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
			}
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(), Messages.getString("MainWindow.Error"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
		}
	}
	
	private void saveFile() { 
		if (fileHandler.hasOpenFile()) {
			try {
				fileHandler.save(this.tree.getRootItem());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), Messages.getString("MainWindow.Error"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
			}
		} else {
			saveFileAs();
		}
	}
	
	private void saveFileAs() {
		if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			try {
				fileHandler.save(this.tree.getRootItem(), fc.getSelectedFile());
				this.setTitle(WINDOW_TITLE + " - " + fileHandler.getCurrentFile().getAbsolutePath());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(this, e.getMessage(), Messages.getString("MainWindow.Error"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
			}
		}
	}
	
	private class EditPanel extends JPanel {
		
		private Item item = null;
		
		private JLabel labelDescription;
		private JLabel labelCreatedDate;
		private JCheckBox checkBoxHasDeadline;
		private JSpinner spinnerDeadlineDate;
		private SpinnerDateModel sdm;
		private JLabel labelDoneOrCancelledDateLabel;
		private JLabel labelDoneOrCancelledDate;
		private JTextArea textAreaMemo;

		private static final int PANEL_WIDTH = 200;
		private static final int PANEL_HEIGHT = 400;

		public void showItem(Item item) {
			if (item != this.item) {
				save();
			}
			this.item = item;
			if (item == null) {
				labelDescription.setText(""); //$NON-NLS-1$
				labelCreatedDate.setText(""); //$NON-NLS-1$
				checkBoxHasDeadline.setSelected(false);
				checkBoxHasDeadline.setEnabled(false);
				spinnerDeadlineDate.setVisible(false);
				labelDoneOrCancelledDate.setText(""); //$NON-NLS-1$
				textAreaMemo.setText(""); //$NON-NLS-1$
				textAreaMemo.setEnabled(false);
			} else {
				labelDescription.setText(item.getDescription());
				labelCreatedDate.setText(nvl(item.getCreationDate(), "").toString()); //$NON-NLS-1$
				checkBoxHasDeadline.setEnabled(true);
				if (item.getDeadline() != null) {
					checkBoxHasDeadline.setSelected(true);
					spinnerDeadlineDate.setVisible(true);
					sdm.setValue(item.getDeadline());
				} else {
					checkBoxHasDeadline.setSelected(false);
					spinnerDeadlineDate.setVisible(false);
					sdm.setValue(new Date());
				}
				if (item.getDone()) {
					labelDoneOrCancelledDateLabel.setVisible(true);
					labelDoneOrCancelledDate.setVisible(true);
					labelDoneOrCancelledDateLabel.setText(Messages.getString("MainWindow.Done") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
					labelDoneOrCancelledDate.setText(nvl(item.getFinishedTime(), "").toString()); //$NON-NLS-1$
				} else if (item.getCancelled()) {
					labelDoneOrCancelledDateLabel.setVisible(true);
					labelDoneOrCancelledDate.setVisible(true);
					labelDoneOrCancelledDateLabel.setText(Messages.getString("MainWindow.Cancelled") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
					labelDoneOrCancelledDate.setText(nvl(item.getCancelledTime(), "").toString()); //$NON-NLS-1$
				} else {
					labelDoneOrCancelledDateLabel.setVisible(false);
					labelDoneOrCancelledDate.setVisible(false);
				}
				textAreaMemo.setText(item.getMemo());
				textAreaMemo.setEnabled(true);
				textAreaMemo.setCaretPosition(0);
			}
		}
		
		public void save() {
			if (this.item != null) {
				if (checkBoxHasDeadline.isSelected()) {
					this.item.setDeadline(sdm.getDate());
				} else {
					this.item.setDeadline(null);
				}
				this.item.setMemo(textAreaMemo.getText());
			}
		}
		
		EditPanel() {

			this.setLayout(new GridBagLayout());
			this.setMinimumSize(new Dimension(PANEL_WIDTH, 10));
					
			final Insets insets = new Insets(5, 5, 5, 5);
			GridBagConstraints gbc = new GridBagConstraints();
			int line = 0;
			
			JPanel panelDescription = new JPanel();
			panelDescription.setBackground(SystemColor.textHighlight);
			panelDescription.setPreferredSize(new Dimension(10, 24));
			FlowLayout fl = new FlowLayout();
			fl.setAlignment(FlowLayout.LEFT);
			panelDescription.setLayout(fl);

			labelDescription = new JLabel(""); //$NON-NLS-1$
			labelDescription.setForeground(SystemColor.textHighlightText);
			labelDescription.setAlignmentX(JLabel.LEFT_ALIGNMENT);
			panelDescription.add(labelDescription);
			gbc = new GridBagConstraints();
			gbc.weightx = 100;
			gbc.weighty = 0;
			gbc.gridx = 0;
			gbc.gridy = line;
			gbc.gridwidth = 3;
			gbc.gridheight = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.anchor = GridBagConstraints.EAST;
			gbc.insets = insets;
			this.add(panelDescription, gbc);
			
			++ line;

			JLabel labelDate1 = new JLabel(Messages.getString("MainWindow.Created_54") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
			gbc = new GridBagConstraints();
			gbc.weightx = 0;
			gbc.weighty = 0;
			gbc.gridx = 0;
			gbc.gridy = line;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.anchor = GridBagConstraints.EAST;
			gbc.insets = insets;
			this.add(labelDate1, gbc);

			labelCreatedDate = new JLabel();
			gbc = new GridBagConstraints();
			gbc.weightx = 100;
			gbc.weighty = 0;
			gbc.gridx = 1;
			gbc.gridy = line;
			gbc.gridwidth = 2;
			gbc.gridheight = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.anchor = GridBagConstraints.WEST;
			gbc.insets = insets;
			this.add(labelCreatedDate, gbc);
			
			++ line;

			JLabel labelDate2 = new JLabel(Messages.getString("MainWindow.Deadline_56") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
			gbc = new GridBagConstraints();
			gbc.weightx = 0;
			gbc.weighty = 0;
			gbc.gridx = 0;
			gbc.gridy = line;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.anchor = GridBagConstraints.EAST;
			gbc.insets = insets;
			this.add(labelDate2, gbc);
			
			checkBoxHasDeadline = new JCheckBox();
			checkBoxHasDeadline.addItemListener(new ItemListener() {
				public void itemStateChanged(ItemEvent e) {
					if (e.getStateChange() == ItemEvent.SELECTED) {
						spinnerDeadlineDate.setVisible(true);
					} else {
						spinnerDeadlineDate.setVisible(false);
					}
				}
			});
			gbc = new GridBagConstraints();
			gbc.weightx = 0;
			gbc.weighty = 0;
			gbc.gridx = 1;
			gbc.gridy = line;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.anchor = GridBagConstraints.EAST;
			gbc.insets = insets;
			this.add(checkBoxHasDeadline, gbc);

			sdm = new SpinnerDateModel();
			spinnerDeadlineDate = new JSpinner(sdm);
			JSpinner.DateEditor de = new JSpinner.DateEditor(spinnerDeadlineDate, "dd MMM yyyy HH:mm:ss"); //$NON-NLS-1$
			spinnerDeadlineDate.setEditor(de); //$NON-NLS-1$
			gbc = new GridBagConstraints();
			gbc.weightx = 100;
			gbc.weighty = 0;
			gbc.gridx = 2;
			gbc.gridy = line;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.anchor = GridBagConstraints.WEST;
			gbc.insets = insets;
			this.add(spinnerDeadlineDate, gbc);
			
			++ line;
			
			labelDoneOrCancelledDateLabel = new JLabel(Messages.getString("MainWindow.Done") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
			gbc = new GridBagConstraints();
			gbc.weightx = 0;
			gbc.weighty = 0;
			gbc.gridx = 0;
			gbc.gridy = line;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.anchor = GridBagConstraints.EAST;
			gbc.insets = insets;
			this.add(labelDoneOrCancelledDateLabel, gbc);

			labelDoneOrCancelledDate = new JLabel();
			gbc = new GridBagConstraints();
			gbc.weightx = 100;
			gbc.weighty = 0;
			gbc.gridx = 1;
			gbc.gridy = line;
			gbc.gridwidth = 2;
			gbc.gridheight = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.anchor = GridBagConstraints.WEST;
			gbc.insets = insets;
			this.add(labelDoneOrCancelledDate, gbc);
			
			++ line;

			JLabel labelNotes = new JLabel(Messages.getString("MainWindow.Notes_61") + ":"); //$NON-NLS-1$ //$NON-NLS-2$
			gbc = new GridBagConstraints();
			gbc.weightx = 0;
			gbc.weighty = 0;
			gbc.gridx = 0;
			gbc.gridy = line;
			gbc.gridwidth = 1;
			gbc.gridheight = 1;
			gbc.anchor = GridBagConstraints.NORTHEAST;
			gbc.insets = insets;
			this.add(labelNotes, gbc);

			textAreaMemo = new JTextArea();
			textAreaMemo.setWrapStyleWord(true);
			textAreaMemo.setLineWrap(true);
			JScrollPane scrollPaneTextAreaMemo = new JScrollPane(textAreaMemo);
			scrollPaneTextAreaMemo.setPreferredSize(new Dimension(10, 100));
			scrollPaneTextAreaMemo.setMinimumSize(scrollPaneTextAreaMemo.getPreferredSize());
			gbc = new GridBagConstraints();
			gbc.weightx = 100;
			gbc.weighty = 100;
			gbc.gridx = 1;
			gbc.gridy = line;
			gbc.gridwidth = 2;
			gbc.gridheight = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.anchor = GridBagConstraints.NORTH;
			gbc.insets = insets;
			this.add(scrollPaneTextAreaMemo, gbc);
			
			showItem(null);
		}
	}
	
	private static Object nvl(Object value, Object defaultValue) {
		return ((value == null) ? defaultValue : value);
	}

}
