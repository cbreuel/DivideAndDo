/*
 * $Id: TaskTree.java,v 1.6 2004/07/22 01:59:14 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/gui/TaskTree.java,v $
 */
 
package net.sf.dad.gui;

import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragGestureRecognizer;
import java.awt.dnd.DragSource;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import net.sf.dad.model.Item;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.6 $
 */
public class TaskTree extends JTree implements
	TreeSelectionListener, TreeExpansionListener,
	DragGestureListener, DropTargetListener, 
	ItemMenuHandler.Listener {
	
	// Instance variable to be used in nested classes
	private TaskTree thisTaskTree = this;
	
	private	TaskTreeNode root;
	private DefaultTreeModel treeModel;
	private TaskTreeNode selectedNode;
	private TaskTreeNode draggedNode;
	private boolean viewDone = false;
	private boolean viewCancelled = false;
	
	private ItemMenuHandler itemMenuHandler;
	
	/**
	 * Creates a new TaskTree.
	 * 
	 */
	public TaskTree() {

		super();
		
		root = new TaskTreeNode(Messages.getString("TaskTree.Tasks_1"), true, this.treeModel, this); //$NON-NLS-1$
		treeModel = new DefaultTreeModel(root);
		super.setModel(treeModel);

		this.setEditable(true);
		this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		this.setCellRenderer(new TaskCellRenderer());
		this.addTreeExpansionListener(this);
		this.addTreeSelectionListener(this);
		
		thisTaskTree.setSelectionPath(new TreePath(root.getPath()));
		selectedNode = root;

		// Popup menu
		
		//itemMenuHandler = new ItemMenuHandler(this);
		itemMenuHandler = ItemMenuHandler.getInstance();
		itemMenuHandler.setListener(this);

		MouseListener popupListener = new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					itemMenuHandler.maybeShowPopup(thisTaskTree, e.getX(), e.getY());
				}
			}
	
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					itemMenuHandler.maybeShowPopup(thisTaskTree, e.getX(), e.getY());
				}
			}
		};
		this.addMouseListener(popupListener);
		
		// Drag-and-Drop
		
		this.setDragEnabled(true);
		DragSource dragSource = DragSource.getDefaultDragSource() ;
		DragGestureRecognizer dgr = 
		  dragSource.createDefaultDragGestureRecognizer(
			this,                             //DragSource
			DnDConstants.ACTION_COPY_OR_MOVE, //specifies valid actions
			this                              //DragGestureListener
		  );
		dgr.setSourceActions(dgr.getSourceActions() & ~InputEvent.BUTTON3_MASK);
		DropTarget dropTarget = new DropTarget(this, this);
	}
	
	/**
	 * @return
	 */
	public Item getRootItem() {
		return this.root.getItem();
	}

	/**
	 * This method display the tree having <code>rootItem</code>
	 * as its root. It's used by File->Open.
	 * 
	 * @param root
	 */
	public void setRootItem(Item rootItem) {

		// Create a new TaskTreeNode to be the root
		reset();
		
		// Sets the Item for the new root
		this.root.setItem(rootItem);
		
		// Calls the recursive rebuildTree in the root to
		// redisplay the tree correctly
		this.root.rebuildTree();
		
		// Sends a reload message do the underliying
		// TreeModel
		this.treeModel.reload();
		
		// Expands the nodes if they are configured as such
		for (int i=0; i<this.getRowCount(); ++i) {
			if (((TaskTreeNode)this.getPathForRow(i).getLastPathComponent()).getExpanded()) {
				this.expandRow(i);
			}
		}
		
		// Request a garbage collection, since we got rid of
		// the old tree
		System.gc();
	}
	
	/**
	 * 
	 */
	public void reload() {
		this.setRootItem(this.root.getItem());
	}
	
	/**
	 * Empties the tree. Used by File->New.
	 */
	public void reset() {
		root = new TaskTreeNode(Messages.getString("TaskTree.Tasks_12"), true, this.treeModel, this); //$NON-NLS-1$
		treeModel = new DefaultTreeModel(root);
		super.setModel(treeModel);
	}
	
	/**
	 * @return
	 */
	public boolean isViewDone() {
		return viewDone;
	}

	/**
	 * @param viewDone
	 */
	public void setViewDone(boolean viewDone) {
		this.viewDone = viewDone;
		this.reload();
	}
	
	/**
	 * @return
	 */
	public boolean isViewCancelled() {
		return viewCancelled;
	}

	/**
	 * @param viewCancelled
	 */
	public void setViewCancelled(boolean viewCancelled) {
		this.viewCancelled = viewCancelled;
		this.reload();
	}
	
	/**
	 * @param isCategory
	 */
	private void addItem(boolean isCategory) {
		if (selectedNode != null) {
			TaskTreeNode childNode = new TaskTreeNode("", isCategory, this.treeModel, this); 
			treeModel.insertNodeInto(childNode, selectedNode,
				selectedNode.getChildCount());
			TreePath childPath = new TreePath(childNode.getPath());
			this.scrollPathToVisible(childPath);
			this.startEditingAtPath(childPath);
		}
	}
	
	private void addItemBelow() {
		if (selectedNode != null && selectedNode.getParent() != null) {
			TaskTreeNode childNode = new TaskTreeNode("", selectedNode.getItem().getIsCategory(), this.treeModel, this); 
			treeModel.insertNodeInto(childNode, (TaskTreeNode)selectedNode.getParent(),
			selectedNode.getParent().getIndex(selectedNode) + 1);
			TreePath childPath = new TreePath(childNode.getPath());
			this.scrollPathToVisible(childPath);
			this.startEditingAtPath(childPath);
		}
	}
	
	/**
	 * @param child
	 * @param offset
	 */
	private void moveChild(TaskTreeNode child, int offset) {
		TaskTreeNode parent = (TaskTreeNode)child.getParent();
		if (parent != null) {
			int newIndex = treeModel.getIndexOfChild(parent, child) + offset;
			if (newIndex >= 0 && newIndex < parent.getChildCount()) {
				child.move(offset);
				treeModel.removeNodeFromParent(child);
				treeModel.insertNodeInto(child, parent, newIndex);
			}
		}
		thisTaskTree.setSelectionPath(new TreePath(child.getPath()));
	}

	
/* **********************************************************************
 * Interface java.awt.dnd.DragGestureListener
 ************************************************************************/ 

	/**
	 * @see java.awt.dnd.DragGestureListener#dragGestureRecognized(java.awt.dnd.DragGestureEvent)
	 */
	public void dragGestureRecognized(DragGestureEvent e) {
		setSelectedItem(e.getDragOrigin().x, e.getDragOrigin().y);
		this.draggedNode = this.selectedNode;
	}


/* **********************************************************************
 * Interface java.awt.dnd.DropTargetListener
 ************************************************************************/ 

	/**
	 * Does nothing.
	 * 
	 * @see java.awt.dnd.DropTargetListener#dragEnter(java.awt.dnd.DropTargetDragEvent)
	 */
	public void dragEnter(DropTargetDragEvent arg0) {
	}

	/**
	 * Sets a node as selected when dragged over.
	 * 
	 * @see java.awt.dnd.DropTargetListener#dragOver(java.awt.dnd.DropTargetDragEvent)
	 */
	public void dragOver(DropTargetDragEvent e) {
		thisTaskTree.setSelectionPath(thisTaskTree.getPathForLocation(e.getLocation().x, e.getLocation().y));
	}

	/**
	 * Does nothing.
	 * 
	 * @see java.awt.dnd.DropTargetListener#dropActionChanged(java.awt.dnd.DropTargetDragEvent)
	 */
	public void dropActionChanged(DropTargetDragEvent arg0) {
	}

	/**
	 * @see java.awt.dnd.DropTargetListener#drop(java.awt.dnd.DropTargetDropEvent)
	 */
	public void drop(DropTargetDropEvent e) {
		TreePath parentPath = this.getPathForLocation(e.getLocation().x, e.getLocation().y);
		if (parentPath != null) {
			if (new TreePath(draggedNode.getPath()).isDescendant(parentPath)) {
				JOptionPane.showMessageDialog(this, Messages.getString("TaskTree.Can_u00B4t_move_a_node_inside_it__s_child._11")); //$NON-NLS-1$
			} else {
				TaskTreeNode newParent = (TaskTreeNode)parentPath.getLastPathComponent();
				draggedNode.delete();
				treeModel.insertNodeInto(draggedNode, newParent, newParent.getChildCount());
				thisTaskTree.scrollPathToVisible(new TreePath(draggedNode.getPath()));
				this.reload();
			}
		}
	}

	/**
	 * Does nothing.
	 * 
	 * @see java.awt.dnd.DropTargetListener#dragExit(java.awt.dnd.DropTargetEvent)
	 */
	public void dragExit(DropTargetEvent arg0) {
	}


/* **********************************************************************
 * Interface javax.swing.event.TreeSelectionListener
 ************************************************************************/ 

	/**
	 * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
	 */
	public void valueChanged(TreeSelectionEvent arg0) {
		this.selectedNode = (TaskTreeNode)arg0.getPath().getLastPathComponent();
	}


/* **********************************************************************
 * Interface javax.swing.event.TreeExpansionListener
 ************************************************************************/

	/**
	 * Sets the <code>Expanded</code> property of the associated <code>Item</code>
	 * to <code>false</code>.
	 * 
	 * @see javax.swing.event.TreeExpansionListener#treeCollapsed(javax.swing.event.TreeExpansionEvent)
	 */
	public void treeCollapsed(TreeExpansionEvent event) {
		((TaskTreeNode)event.getPath().getLastPathComponent()).setExpanded(false);
	}

	/**
	 * Sets the <code>Expanded</code> property of the associated <code>Item</code>
	 * to <code>true</code>.
	 * 
	 * @see javax.swing.event.TreeExpansionListener#treeExpanded(javax.swing.event.TreeExpansionEvent)
	 */
	public void treeExpanded(TreeExpansionEvent event) {
		((TaskTreeNode)event.getPath().getLastPathComponent()).setExpanded(true);
	}


/* **********************************************************************
 * Interface net.sf.dad.gui.ItemMenuHandler.Listener
 ************************************************************************/

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#newTask()
	 */
	public void newTask() {
   		addItem(false);
   	}
    
	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#newCategory()
	 */
	public void newCategory() {
   		addItem(true);
   	}
    
	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#newItemBelow()
	 */
	public void newItemBelow() {
		addItemBelow();
	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#delete()
	 */
	public void delete() {
   		selectedNode.delete();
   	}
    
	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#moveUp()
	 */
	public void moveUp() {
   		moveChild(selectedNode, -1);
   	}
 
	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#moveDown()
	 */
	public void moveDown() {
  		moveChild(selectedNode, +1);
   	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#done()
	 */
	public void done(boolean b) {
		selectedNode.setDone(b);
	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#cancelled()
	 */
	public void cancelled(boolean b) {
		selectedNode.setCancelled(b);
	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#setSelectedItem(int, int)
	 */
	public void setSelectedItem(int x, int y) {
		TreePath path = this.getPathForLocation(x, y);
		if (path == null) {
			selectedNode = null;
		} else {
			this.setSelectionPath(path);
			selectedNode = (TaskTreeNode)path.getLastPathComponent();
		}
	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#isCurrentItemRoot()
	 */
	public boolean isCurrentItemRoot() {
		return this.selectedNode == root;
	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#isCurrentItemDone()
	 */
	public boolean isCurrentItemDone() {
		return this.selectedNode.getItem().getDone();
	}

	/* (non-Javadoc)
	 * @see net.sf.dad.gui.ItemMenuHandler.Listener#isCurrentItemCancelled()
	 */
	public boolean isCurrentItemCancelled() {
		return this.selectedNode.getItem().getCancelled();
	}

}
