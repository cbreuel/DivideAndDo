/*
 * $Id: ItemMenuHandler.java,v 1.3 2004/07/22 02:04:29 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/gui/ItemMenuHandler.java,v $
 */
 
package net.sf.dad.gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

/**
 * This class creates the Item menu in two versions: JMenu and JPopupMenu.
 * This is necessary because, although the JMenu class uses a JPopupMenu
 * internally, there is no method to set this property, making it hard to
 * reuse a JPopupMenu as a JMenu.
 * 
 * It's implemented as a singleton.
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.3 $
 */
public class ItemMenuHandler {
	
	private JPopupMenu popup;
	private JMenu menu;

	private JCheckBoxMenuItem menuDoneP;
	private JCheckBoxMenuItem menuCancelledP;
	private JMenuItem menuDeleteP;
	private JMenuItem menuMoveUpP;
	private JMenuItem menuMoveDownP;
	private JCheckBoxMenuItem menuDoneM;
	private JCheckBoxMenuItem menuCancelledM;
	private JMenuItem menuDeleteM;
	private JMenuItem menuMoveUpM;
	private JMenuItem menuMoveDownM;
	
	private Listener listener;
	
	private static ItemMenuHandler instance = null;
	
	public static ItemMenuHandler getInstance() {
		if (instance == null) {
			instance = new ItemMenuHandler();
		}

		return instance;
	}

	private ItemMenuHandler() {
		
		popup = new JPopupMenu();
		menu = new JMenu(Messages.getString("MainWindow.Item"));

		menu.setMnemonic(KeyEvent.VK_I);
		
		popup.add(createMenuAdd());
		menu.add(createMenuAdd());

		menuDeleteP = createMenuDelete();
		popup.add(menuDeleteP);
		popup.addSeparator();

		menuDeleteM = createMenuDelete();
		menu.add(menuDeleteM);
		menu.addSeparator();

		menuMoveUpP = createMenuMoveUp();
		popup.add(menuMoveUpP);

		menuMoveUpM = createMenuMoveUp();
		menu.add(menuMoveUpM);

		menuMoveDownP = createMenuMoveDown();
		popup.add(menuMoveDownP);
		popup.addSeparator();

		menuMoveDownM = createMenuMoveDown();
		menu.add(menuMoveDownM);
		menu.addSeparator();

		menuDoneP = createMenuDone();
		popup.add(menuDoneP);
		menuDoneM = createMenuDone();
		menu.add(menuDoneM);

		menuCancelledP = createMenuCancelled();
		popup.add(menuCancelledP);
		menuCancelledM = createMenuCancelled();
		menu.add(menuCancelledM);
		
		menu.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent e) {
			}
			public void menuDeselected(MenuEvent e) {
			}
			public void menuSelected(MenuEvent e) {
				setItemStates();
			}
		});

	}
	
	public void setListener(ItemMenuHandler.Listener listener) {
		this.listener = listener;
	}
	
	private JMenu createMenuAdd() {
		
		JMenu retval;
		
		retval = new JMenu(Messages.getString("TaskTree.Add_2"));
		JMenuItem subMenuItem;
		subMenuItem = new JMenuItem(Messages.getString("TaskTree.Task_3")); //$NON-NLS-1$
		subMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.newTask();
			}
		});
		subMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
		retval.add(subMenuItem);

		subMenuItem = new JMenuItem(Messages.getString("TaskTree.Category_4")); //$NON-NLS-1$
		subMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.newCategory();
			}
		});
		subMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
		retval.add(subMenuItem);
		
		subMenuItem = new JMenuItem(Messages.getString("TaskTree.Item_below")); //$NON-NLS-1$
		subMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.newItemBelow();
			}
		});
		subMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0));
		retval.add(subMenuItem);
		
		return retval;
	}
	
	private JMenuItem createMenuDelete() {

		JMenuItem retval;
		
		retval = new JMenuItem(Messages.getString("TaskTree.Delete_5"));
		retval.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.delete();
			}
		});
		
		retval.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0));

		return retval;
	}
	
	private JMenuItem createMenuMoveUp() {

		JMenuItem retval;
		
		retval = new JMenuItem(Messages.getString("TaskTree.Move_up_6"));
		retval.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.moveUp();
			}
		});
		
		retval.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));

		return retval;
	}
	
	private JMenuItem createMenuMoveDown() {

		JMenuItem retval;
		
		retval = new JMenuItem(Messages.getString("TaskTree.Move_down_7"));
		retval.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listener.moveDown();
			}
		});
			
		retval.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));

		return retval;
	}


	private JCheckBoxMenuItem createMenuDone() {

		JCheckBoxMenuItem retval;
		
		retval = new JCheckBoxMenuItem(Messages.getString("TaskTree.Done_8"));
		retval.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				listener.done(e.getStateChange() == ItemEvent.SELECTED);
			}
		});
			
		return retval;
	}

	private JCheckBoxMenuItem createMenuCancelled() {

		JCheckBoxMenuItem retval;
			
		retval = new JCheckBoxMenuItem(Messages.getString("TaskTree.Cancelled_9"));
		retval.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				listener.cancelled(e.getStateChange() == ItemEvent.SELECTED);
			}
		});

		return retval;
	}
		
	public void maybeShowPopup(Component c, int x, int y) {
		listener.setSelectedItem(x, y);
		setItemStates();
		popup.show(c, x, y);
	}
	
	private void setItemStates() {
		if (listener.isCurrentItemRoot()) {
			menuDeleteP.setEnabled(false);
			menuDoneP.setEnabled(false);
			menuCancelledP.setEnabled(false);
			menuMoveUpP.setEnabled(false);
			menuMoveDownP.setEnabled(false);
			menuDoneP.setSelected(false);
			menuCancelledP.setSelected(false);
			menuDeleteM.setEnabled(false);
			menuDoneM.setEnabled(false);
			menuCancelledM.setEnabled(false);
			menuMoveUpM.setEnabled(false);
			menuMoveDownM.setEnabled(false);
			menuDoneM.setSelected(false);
			menuCancelledM.setSelected(false);
		} else {
			menuDeleteP.setEnabled(true);
			menuDoneP.setEnabled(true);
			menuCancelledP.setEnabled(true);
			menuMoveUpP.setEnabled(true);
			menuMoveDownP.setEnabled(true);
			menuDoneP.setSelected(listener.isCurrentItemDone());
			menuCancelledP.setSelected(listener.isCurrentItemCancelled());
			menuDeleteM.setEnabled(true);
			menuDoneM.setEnabled(true);
			menuCancelledM.setEnabled(true);
			menuMoveUpM.setEnabled(true);
			menuMoveDownM.setEnabled(true);
			menuDoneM.setSelected(listener.isCurrentItemDone());
			menuCancelledM.setSelected(listener.isCurrentItemCancelled());
		}
	}

	public JMenu getMenu() {
		return this.menu;
	}

	public JPopupMenu getPopupMenu() {
		return this.popup;
	}
	
	public interface Listener {
		public void newTask();
		public void newCategory();
		public void newItemBelow();
		public void delete();
		public void moveUp();
		public void moveDown();
		public void done(boolean b);
		public void cancelled(boolean b);
		public void setSelectedItem(int x, int y);
		public boolean isCurrentItemRoot();
		public boolean isCurrentItemDone();
		public boolean isCurrentItemCancelled();
	}

}
