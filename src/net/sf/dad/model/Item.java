/*
 * $Id: Item.java,v 1.2 2003/07/29 22:17:29 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/model/Item.java,v $
 */

package net.sf.dad.model;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.2 $
 */
public class Item {

	private boolean isCategory;
	
	private boolean expanded;

	private boolean done;
	private boolean cancelled;

	private String description;
	private String memo;
	private Date creationDate = new Date();
	private Date deadline;
	private Date finishedTime;
	private Date cancelledTime;

	private Item parent;
	private List children = new LinkedList();

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMemo() {
		return memo;
	}

	public void setParent(Item parent) {
		this.parent = parent;
	}

	public Item getParent() {
		return parent;
	}

	public Item addChild() {
		Item retval = new Item();

		retval.setParent(this);
		this.children.add(retval);

		return retval;
	}

	public void addChild(Item child) {
		child.setParent(this);
		this.children.add(child);
	}

	public Item insertChild(int index) {
		Item retval = new Item();

		retval.setParent(this);
		this.children.add(index, retval);

		return retval;
	}

	public void insertChild(int index, Item child) {
		if (!this.children.contains(child)) {
			child.setParent(this);
			this.children.add(index, child);
		}
	}

	public void removeChild(Item child) {
		this.children.remove(child);
	}
	
	public boolean hasChildren() {
		return !this.children.isEmpty();
	}

	public void setDone(boolean done) {
		this.done = done;
		this.finishedTime = new Date();
	}

	public boolean getDone() {
		return done;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
		this.cancelledTime = new Date();
	}

	public boolean getCancelled() {
		return cancelled;
	}

	/**
	 * @param b
	 */
	public void setIsCategory(boolean b) {
		isCategory = b;
	}

	/**
	 * @return
	 */
	public boolean getIsCategory() {
		return isCategory;
	}

	/**
	 * @return
	 */
	public Iterator childrenIterator() {
		return children.iterator();
	}
	
	public Item[] getChildren() {
		return (Item[])this.children.toArray(new Item[0]); 
	}

	public void setChildren(Item[] children) {
		this.children.clear();
		for (int i=0; i<children.length; ++i) {
			this.children.add(children[i]);
		}
	}

	/**
	 * Returns the item's description. 
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.getDescription();
	}

	/**
	 * 
	 */
	public void removeFromParent() {
		this.parent.removeChild(this);
		this.parent = null;
	}
	
	/**
	 * @return
	 */
	public Date getCancelledTime() {
		return cancelledTime;
	}

	/**
	 * @return
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * @return
	 */
	public Date getDeadline() {
		return deadline;
	}

	/**
	 * @return
	 */
	public Date getFinishedTime() {
		return finishedTime;
	}

	/**
	 * @param date
	 */
	public void setCancelledTime(Date date) {
		cancelledTime = date;
	}

	/**
	 * @param date
	 */
	public void setCreationDate(Date date) {
		creationDate = date;
	}

	/**
	 * @param date
	 */
	public void setDeadline(Date date) {
		deadline = date;
	}

	/**
	 * @param date
	 */
	public void setFinishedTime(Date date) {
		finishedTime = date;
	}

	/**
	 * @return
	 */
	public boolean getExpanded() {
		return expanded;
	}

	/**
	 * @param b
	 */
	public void setExpanded(boolean b) {
		expanded = b;
	}

}
