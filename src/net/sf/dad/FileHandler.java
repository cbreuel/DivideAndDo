/*
 * $Id: FileHandler.java,v 1.3 2004/07/22 02:05:14 cmbreuel Exp $
 * $Source: /cvsroot-fuse/dad/DAD/src/net/sf/dad/FileHandler.java,v $
 */
 
package net.sf.dad;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import net.sf.dad.model.Item;

/**
 * 
 * @author $Author: cmbreuel $
 * @version $Revision: 1.3 $
 */
public class FileHandler {
	
	private File currentFile = null;
	
	public static final String DEFAULT_EXTENSION = "dad";
	public static final String COMPRESSED_EXTENSION = "zdad";
	
	public void save(Item root, File f) throws IOException {
		if (f.getName().toLowerCase().endsWith("." + FileHandler.COMPRESSED_EXTENSION)
				|| f.getName().toLowerCase().endsWith("." + FileHandler.DEFAULT_EXTENSION)) {
			currentFile = f;
		} else {
			currentFile  = new File(f.getAbsolutePath() + "." + FileHandler.COMPRESSED_EXTENSION);
		}
		save(root);
	}

	public void save(Item root) throws IOException {

		XMLEncoder encoder = null;
		OutputStream outFile = null;
		
		if (currentFile.getName().toLowerCase().endsWith("." + FileHandler.DEFAULT_EXTENSION)) {
			outFile = new FileOutputStream(currentFile);
		} else {
			outFile = new ZipOutputStream(new FileOutputStream(currentFile));
			((ZipOutputStream)outFile).putNextEntry(
				new ZipEntry(
					currentFile.getName().replaceAll("\\." + COMPRESSED_EXTENSION, "\\." + DEFAULT_EXTENSION)));
		}

		encoder = new XMLEncoder(outFile);
		encoder.writeObject(root);
		encoder.close();
		outFile.close();
		
	}
	
	public Item open(File f) throws FileNotFoundException, IOException {
		
		Item retval = null;
		XMLDecoder decoder = null;
		InputStream inFile = null;

		if (f.getName().toLowerCase().endsWith("." + COMPRESSED_EXTENSION)) {
			inFile = new ZipInputStream(new FileInputStream(f));
			((ZipInputStream)inFile).getNextEntry();
		} else {
			inFile = new FileInputStream(f);
		}

		decoder = new XMLDecoder(inFile);
		retval = (Item)decoder.readObject();
		
		currentFile = f;
		
		return retval;

	}
	
	public void newFile() {
		currentFile = null;
	}
	
	public boolean hasOpenFile() {
		return currentFile != null;
	}

	/**
	 * @return
	 */
	public File getCurrentFile() {
		return currentFile;
	}

}
